const Role = require('../models/role');
const Usuario = require('../models/usuario');

const esRoleValido = async (rol = '') => {
    const existRol = await Role.findOne({ rol });

    if ( !existRol ) {
        throw new Error(`El rol ${ rol } no está registrado en la BD`);
    }
}

const emailExiste = async (correo = '') => {
    // Verificar si el correo existe
    const existEmail = await Usuario.findOne({ correo })
    if( existEmail ) {
        throw new Error(`El correo ${ correo } ya existe en la base de datos`);
    }
}

const existeUsuarioPorId = async (id) => {
    // Verificar si el id existe
    const existUsuario = await Usuario.findById(id)
    if( !existUsuario ) {
        throw new Error(`El id no existe: ${ id }`);
    }
}

module.exports = {
    esRoleValido,
    emailExiste,
    existeUsuarioPorId
}