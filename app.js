/*
    La prioridad de importaciones (Orden) va de la siguiente forma:

    - Importaciones propias de node
    - Importaciones de Terceros
    - Importaciones propias
*/

require('dotenv').config();

const Server = require('./models/server');

const server = new Server();

server.listen();
 
