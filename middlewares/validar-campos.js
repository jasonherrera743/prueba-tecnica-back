const { validationResult } = require('express-validator');

const validarCampos = ( req, res, next ) => {

    const errors = validationResult(req);
    if ( !errors.isEmpty() ){
        return res.status(400).json(errors);
    }

    next(); // Si llega a este punto sigue con el siguiente middleware o sale al controlador
}

module.exports = {
    validarCampos
}